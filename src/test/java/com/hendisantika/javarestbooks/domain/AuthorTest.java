package com.hendisantika.javarestbooks.domain;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by IntelliJ IDEA.
 * Project : java-rest-books
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/01/18
 * Time: 06.20
 * To change this template use File | Settings | File Templates.
 */
public class AuthorTest {
    @Test
    public void should_return_to_string() {

        // Given
        final Author author = new Author("Uzumaki", "Naruto");

        // When
        final String toString = author.toString();

        // Then
        assertThat(toString, is("Author[firstName=Uzumaki,lastName=Naruto]"));
    }
}
package com.hendisantika.javarestbooks.domain;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by IntelliJ IDEA.
 * Project : java-rest-books
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/01/18
 * Time: 06.21
 * To change this template use File | Settings | File Templates.
 */
public class BookTest {
    @Test
    public void should_return_to_string() {

        // Given
        final Author author = new Author("Uzumaki", "Naruto");
        final Book book = new Book("isbn", "title", "publisher");
        book.addAuthor(author);

        StringBuilder expectedString = new StringBuilder("Book[id=<null>,isbn=isbn,title=title,description=<null>,");
        expectedString.append("authors=[Author[firstName=Uzumaki,lastName=Naruto]],publisher=publisher]");

        // When
        final String toString = book.toString();

        // Then
        assertThat(toString, is(expectedString.toString()));
    }
}
package com.hendisantika.javarestbooks.repository;

import com.hendisantika.javarestbooks.domain.Book;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : java-rest-books
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/01/18
 * Time: 06.07
 * To change this template use File | Settings | File Templates.
 */
public interface BookRepository extends PagingAndSortingRepository<Book, Long> {
    Optional<Book> findByIsbn(String isbn);
}
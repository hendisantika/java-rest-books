package com.hendisantika.javarestbooks.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : java-rest-books
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/01/18
 * Time: 06.09
 * To change this template use File | Settings | File Templates.
 */
public class BookIsbnAlreadyExistsException extends RuntimeException {

    public BookIsbnAlreadyExistsException(String isbn) {
        super("book already exists for ISBN: '" + isbn + "'");
    }
}
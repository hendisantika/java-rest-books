package com.hendisantika.javarestbooks.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : java-rest-books
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/01/18
 * Time: 06.10
 * To change this template use File | Settings | File Templates.
 */
public class BookNotFoundException extends RuntimeException {

    public BookNotFoundException(String isbn) {
        super("could not find book with ISBN: '" + isbn + "'");
    }
}